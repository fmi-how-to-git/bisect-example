#!/bin/bash

eps=0.001
expected=23.2551

dist=$(go run main.go -o 5 -l 80 | awk '{ print $5 }')

if (( $(echo "${dist} > ${expected} + ${eps}" | bc -l) )); then
  echo "ERROR! Expected ${expected} but got ${dist}"
  exit 1
fi

if (( $(echo "${dist} < ${expected} - ${eps}" | bc -l) )); then
  echo "ERROR! Expected ${expected} but got ${dist}"
  exit 1
fi
