package main

import (
	"flag"
	"fmt"
	"math"
)

var (
	// observerHeight is the height of the observer at sea in meters.
	observerHeight float64

	// landObjectHeight is the height of landmark on the coastline in meters.
	landObjectHeight float64
)

func init() {
	flag.Float64Var(&observerHeight, "o", 5, "Height of the observer at sea in meters")
	flag.Float64Var(&landObjectHeight, "l", 64, "Height of landmark on the coastline in meters")
}

func main() {
	flag.Parse()
	dist := 2.08*math.Sqrt(observerHeight/nmInMeters) + 2.08*math.Sqrt(landObjectHeight/nmInMeters)
	fmt.Println("Distance to landmark is", dist, "nautical miles")
}

// nmInMeters is the length of a nautical miles in SI meters.
const nmInMeters = 1852
