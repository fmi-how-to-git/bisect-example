# Bisect Demo

This is an artifical repository with an example of how `git bisect` is useful.

## What Does It Do?

The example program calculates the furthest distance at which a landmark on the
coastline will be visible to an observer at sea.

Note that the program **is not correct** on purpose. It is supposed to demonstrate
the `git bisect` command.

## Bisect How To

```
git bisect start
git bisect bad
git bisect good 5eab26d
git bisect run ./test.sh
```
